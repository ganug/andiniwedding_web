<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Nugroho Dhyni &mdash; Wedding</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Dian Nugroho and Dini Khusnul Wedding Premium Responsive Template" />
	<meta name="keywords" content="Dian Nugroho,Nugroho,ganug,dini khusnul,dini,nudhy wedding" />
	<meta name="author" content="ArduiNug" />
	<link rel="shortcut icon" href="images/dd_logo.png">

	<!-- Facebook and Twitter integration -->
	<meta property="og:title" content="" />
	<meta property="og:image" content="" />
	<meta property="og:url" content="" />
	<meta property="og:site_name" content="" />
	<meta property="og:description" content="" />
	<meta name="twitter:title" content="" />
	<meta name="twitter:image" content="" />
	<meta name="twitter:url" content="" />
	<meta name="twitter:card" content="" />

	<link href='https://fonts.googleapis.com/css?family=Work+Sans:400,300,600,400italic,700' rel='stylesheet' type='text/css'>
	<link href="https://fonts.googleapis.com/css?family=Sacramento" rel="stylesheet">

	<!-- Animate.css -->
	<link rel="stylesheet" href="css/animate.css">
	<!-- Icomoon Icon Fonts-->
	<link rel="stylesheet" href="css/icomoon.css">
	<!-- Bootstrap  -->
	<link rel="stylesheet" href="css/bootstrap.css">

	<!-- Magnific Popup -->
	<link rel="stylesheet" href="css/magnific-popup.css">

	<!-- Owl Carousel  -->
	<link rel="stylesheet" href="css/owl.carousel.min.css">
	<link rel="stylesheet" href="css/owl.theme.default.min.css">

	<!-- Theme style  -->
	<link rel="stylesheet" href="css/style.css">

	<!-- Modernizr JS -->
	<script src="js/modernizr-2.6.2.min.js"></script>
	<!-- FOR IE9 below -->
	<!--[if lt IE 9]>
	<script src="js/respond.min.js"></script>
	<![endif]-->

</head>

<body>
	<audio autoplay>
		<source src="audio/audio1.mp3" type="audio/mp3">
		Your browser does not support the audio element.


	</audio>
	<div class="fh5co-loader"></div>

	<div id="page">
		<nav class="fh5co-nav" role="navigation">
			<div class="container">
				<div class="row">
					<div class="col-xs-4">
						<div id="fh5co-logo"><a href="index.html">Andini Wedding<strong>.</strong></a></div>
					</div>
					<!-- <div class="col-xs-10 text-right menu-1">
						<ul>
							<li class="active"><a href="index.html">Home</a></li>
							<li><a href="about.html">Story</a></li>
							<li class="has-dropdown">
								<a href="services.html">Services</a>
								<ul class="dropdown">
									<li><a href="#">Web Design</a></li>
									<li><a href="#">eCommerce</a></li>
									<li><a href="#">Branding</a></li>
									<li><a href="#">API</a></li>
								</ul>
							</li>
							<li class="has-dropdown">
								<a href="gallery.html">Gallery</a>
								<ul class="dropdown">
									<li><a href="#">HTML5</a></li>
									<li><a href="#">CSS3</a></li>
									<li><a href="#">Sass</a></li>
									<li><a href="#">jQuery</a></li>
								</ul>
							</li>
							<li><a href="contact.html">Contact</a></li>
						</ul>
					</div> -->
				</div>

			</div>
		</nav>

		<header id="fh5co-header" class="fh5co-cover" role="banner" style="background-image:url(images/pinus/img001.JPG);" data-stellar-background-ratio="0.5">
			<div class="overlay"></div>
			<div class="container">
				<div class="row">
					<div class="col-md-8 col-md-offset-2 text-center">
						<div class="display-t">
							<div class="display-tc animate-box" data-animate-effect="fadeIn">
								<!-- <h1>Dian Nugroho <br />&amp; Dhini Khusnul</h1> -->
								<h1>Dian &amp; Dini </h1>
								<h2>We Are Getting Married</h2>
								<div class="simply-countdown simply-countdown-one"></div>
								<?php
								if (!isset($_GET['mobile'])) {
									echo '<p><a href="/download/DianDiniWedding.apk" class="btn btn-default btn-sm">Download Apps</a></p>';
								}
								?>
							</div>
						</div>

					</div>
				</div>
			</div>
		</header>

		<div id="fh5co-couple">
			<div class="container">
				<div class="row">
					<div class="col-md-8 col-md-offset-2 text-center fh5co-heading animate-box">
						<h2>Hello!</h2>
						<h3>November 23th, 2020 Mojokerto</h3>
						<p>We invited you to celebrate our wedding</p>
					</div>
				</div>
				<div class="couple-wrap animate-box">
					<div class="couple-half">
						<div class="groom">
							<img src="images/pinus/ganug.JPG" alt="groom" class="img-responsive">
						</div>
						<div class="desc-groom">
							<h3>Dian Nugroho, S.Kom</h3> <br />
							<p>Putra Bapak Sukamko & Ibu Utami, <br />
								Sekar Putih Mojokerto</p>
						</div>
					</div>
					<p class="heart text-center"><i class="icon-heart2"></i></p>
					<div class="couple-half">
						<div class="bride">
							<img src="images/pinus/IMG_4349.JPG" alt="groom" class="img-responsive">
						</div>
						<div class="desc-bride">
							<h3>Dini Khusnul Siti Khotimah S.Pd</h3>
							<p>Putri Bapak Mujiono & Ibu Wiwik,<br />
								Dusun Sonosari</p>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div id="fh5co-event" class="fh5co-bg" style="background-image:url(images/img_bg_3.jpg);">
			<div class="overlay"></div>
			<div class="container">
				<div class="row">
					<div class="col-md-8 col-md-offset-2 text-center fh5co-heading animate-box">
						<span>Our Special Events</span>
						<h2>Wedding Events</h2>
					</div>
				</div>
				<div class="row">
					<div class="display-t">
						<div class="display-tc">
							<div class="col-md-10 col-md-offset-1">
								<div class="col-md-6 col-sm-6 text-center">
									<div class="event-wrap animate-box">
										<h3>Main Ceremony</h3>
										<div class="event-col">
											<i class="icon-clock"></i>
											<span>10:00 PM</span>
											<span>12:00 PM</span>
										</div>
										<div class="event-col">
											<i class="icon-calendar"></i>
											<span>Monday 23</span>
											<span>November, 2020</span>
										</div>
										<!-- <p>Far far away, behind the word mountains, far from the countries Vokalia and
											Consonantia, there live the blind texts. Separated they live in
											Bookmarksgrove right at the coast of the Semantics, a large language ocean.
										</p> -->
									</div>
								</div>
								<div class="col-md-6 col-sm-6 text-center">
									<div class="event-wrap animate-box">
										<h3>Wedding Party</h3>
										<div class="event-col">
											<i class="icon-clock"></i>
											<span>13:00 PM</span>
											<span>23:59 AM</span>
										</div>
										<div class="event-col">
											<i class="icon-calendar"></i>
											<span>Monday 23</span>
											<span>November, 2020</span>
										</div>
										<!-- <p>Far far away, behind the word mountains, far from the countries Vokalia and
											Consonantia, there live the blind texts. Separated they live in
											Bookmarksgrove right at the coast of the Semantics, a large language ocean.
										</p> -->
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div id="fh5co-couple-story">
			<div class="container">
				<div class="row">
					<div class="col-md-8 col-md-offset-2 text-center fh5co-heading animate-box">
						<span>We Love Each Other</span>
						<h2>Our Story</h2>
						<p>our story journey. beginning from a meeting at school until we decided to get married.</p>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12 col-md-offset-0">
						<ul class="timeline animate-box">
							<li class="animate-box">
								<div class="timeline-badge" style="background-image:url(images/couple-1.jpg);"></div>
								<div class="timeline-panel">
									<div class="timeline-heading">
										<h3 class="timeline-title">First We Meet</h3>
										<span class="date">November 25, 2012</span>
									</div>
									<div class="timeline-body">
										<!-- <p>Far far away, behind the word mountains, far from the countries Vokalia and
											Consonantia, there live the blind texts. Separated they live in
											Bookmarksgrove right at the coast of the Semantics, a large language ocean.
										</p> -->
									</div>
								</div>
							</li>
							<li class="timeline-inverted animate-box">
								<div class="timeline-badge" style="background-image:url(images/couple-2.jpg);"></div>
								<div class="timeline-panel">
									<div class="timeline-heading">
										<h3 class="timeline-title">First Date</h3>
										<span class="date">December 31, 2012</span>
									</div>
									<div class="timeline-body">
										<!-- <p>
											Benteng Pancasila, jalan yang namanya sangat familiar di Kota Mojokerto ini,
											adalah tempat pertama dimana aku memberanikan diri mengajak calon istriku
											bertemu
											untuk bersama-sama merayakan tahun baru 2013. Tahun baru dan cerita yang
											baru pastinya. haha
											tahun dimana kita lebih dekat satu sama lain.
										</p> -->
									</div>
								</div>
							</li>
							<li class="animate-box">
								<div class="timeline-badge" style="background-image:url(images/couple-3.jpg);"></div>
								<div class="timeline-panel">
									<div class="timeline-heading">
										<h3 class="timeline-title">In A Relationship</h3>
										<span class="date">January 3, 2013</span>
									</div>
									<div class="timeline-body">
										<!-- <p>Far far away, behind the word mountains, far from the countries Vokalia and
											Consonantia, there live the blind texts. Separated they live in
											Bookmarksgrove right at the coast of the Semantics, a large language ocean.
										</p> -->
									</div>
								</div>
							</li>
							<li class="timeline-inverted animate-box">
								<div class="timeline-badge" style="background-image:url(images/couple-4.jpg);"></div>
								<div class="timeline-panel">
									<div class="timeline-heading">
										<h3 class="timeline-title">Engagement</h3>
										<span class="date">April 14, 2020</span>
									</div>
									<div class="timeline-body">
										<!-- <p>
											Benteng Pancasila, jalan yang namanya sangat familiar di Kota Mojokerto ini,
											adalah tempat pertama dimana aku memberanikan diri mengajak calon istriku
											bertemu
											untuk bersama-sama merayakan tahun baru 2013. Tahun baru dan cerita yang
											baru pastinya. haha
											tahun dimana kita lebih dekat satu sama lain.
										</p> -->
									</div>
								</div>
							</li>

						</ul>
					</div>
				</div>
			</div>
		</div>

		<div id="fh5co-gallery" class="fh5co-section-gray">
			<div class="container">
				<div class="row">
					<div class="col-md-8 col-md-offset-2 text-center fh5co-heading animate-box">
						<span>Our Memories</span>
						<h2>Wedding Gallery</h2>
						<!-- <p>Our Weeding Photos Gallery.</p> -->
					</div>
				</div>
				<div class="row row-bottom-padded-md">
					<div class="col-md-12">
						<ul id="fh5co-gallery-list">

							<li class="one-third animate-box" data-animate-effect="fadeIn" style="background-image: url(images/gallery-1.jpg); ">
								<a href="images/gallery-1.jpg">
									<div class="case-studies-summary">
										<!-- <span>14 Photos</span> -->
										<!-- <h2>Two Glas of Juice</h2> -->
									</div>
								</a>
							</li>
							<li class="one-third animate-box" data-animate-effect="fadeIn" style="background-image: url(images/gallery-2.jpg); ">
								<a href="#" class="color-2">
									<div class="case-studies-summary">
										<!-- <span>30 Photos</span> -->
										<!-- <h2>Timer starts now!</h2> -->
									</div>
								</a>
							</li>


							<li class="one-third animate-box" data-animate-effect="fadeIn" style="background-image: url(images/couple-3.jpg); ">
								<a href="#" class="color-3">
									<div class="case-studies-summary">
										<!-- <span>90 Photos</span> -->
										<!-- <h2>Beautiful sunset</h2> -->
									</div>
								</a>
							</li>
							<li class="one-third animate-box" data-animate-effect="fadeIn" style="background-image: url(images/gallery-4.jpg); ">
								<a href="#" class="color-4">
									<div class="case-studies-summary">
										<!-- <span>12 Photos</span> -->
										<!-- <h2>Company's Conference Room</h2> -->
									</div>
								</a>
							</li>

							<li class="one-third animate-box" data-animate-effect="fadeIn" style="background-image: url(images/gallery-5.jpg); ">
								<a href="#" class="color-3">
									<div class="case-studies-summary">
										<!-- <span>50 Photos</span> -->
										<!-- <h2>Useful baskets</h2> -->
									</div>
								</a>
							</li>
							<li class="one-third animate-box" data-animate-effect="fadeIn" style="background-image: url(images/gallery-6.jpg); ">
								<a href="#" class="color-4">
									<div class="case-studies-summary">
										<!-- <span>45 Photos</span> -->
										<!-- <h2>Skater man in the road</h2> -->
									</div>
								</a>
							</li>

							<li class="one-third animate-box" data-animate-effect="fadeIn" style="background-image: url(images/gallery-7.jpg); ">
								<a href="#" class="color-4">
									<div class="case-studies-summary">
										<!-- <span>35 Photos</span> -->
										<!-- <h2>Two Glas of Juice</h2> -->
									</div>
								</a>
							</li>

							<li class="one-third animate-box" data-animate-effect="fadeIn" style="background-image: url(images/gallery-8.jpg); ">
								<a href="#" class="color-5">
									<div class="case-studies-summary">
										<!-- <span>90 Photos</span> -->
										<!-- <h2>Timer starts now!</h2> -->
									</div>
								</a>
							</li>
							<li class="one-third animate-box" data-animate-effect="fadeIn" style="background-image: url(images/gallery-9.jpg); ">
								<a href="#" class="color-6">
									<div class="case-studies-summary">
										<!-- <span>56 Photos</span> -->
										<!-- <h2>Beautiful sunset</h2> -->
									</div>
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<header id="fh5co-header" class="fh5co-cover" role="banner" style="background-image:url(images/pinus/IMG_4363_2.png);  background-repeat: no-repeat;
			background-size: 100% 100%;" data-stellar-background-ratio="0.5">
			<div class="overlay"></div>
			<div class="container">
				<div class="row">
					<div class="col-md-8 col-md-offset-2 text-center">
						<div class="display-t">

						</div>
					</div>
				</div>
			</div>
		</header>


		<div id="fh5co-testimonial">
			<div class="container">
				<div class="row">
					<div class="row animate-box">
						<div class="col-md-8 col-md-offset-2 text-center fh5co-heading">
							<span>Wishes</span>
							<h2>Friends Wishes</h2>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12 animate-box">
							<div class="wrap-testimony">
								<div class="owl-carousel-fullwidth autoplay autoplay-control">
									<div class="item">
										<div class="testimony-slide text-center">
											<figure>
												<img src="images/friend_wish/wulandari.png" alt="user">
											</figure>
											<span>Wulandari, via <a class="twitter">Whats App</a></span>
											<blockquote>
												<p>
													Selamat buat Mb Brow dan Dian semoga lancar sampai hari H tanpa ada suatu halangan apapun.Barakallahu laka wa baroka alaika wa jama’a Bainakumaa fii khoirin (Semoga Allah memberkahi engkau dalam segala hal (yang baik) dan mempersatukan kamu berdua dalam kebaikan.
												</p>
											</blockquote>
										</div>
									</div>
									<div class="item">
										<div class="testimony-slide text-center">
											<figure>
												<img src="images/friend_wish/jarwo.png" alt="user">
											</figure>
											<span>Abdullah, via <a class="twitter">Kantor POS</a></span>
											<blockquote>
												<p>
													Selamat menjalani hidup baru. Semoga pernikahan kalian langgeng dan selalu mendapatkan petunjuk Allah Swt dalam setiap langkah. Manusia diciptakan Allah Swt berpasang-pasangan. Itulah takdir dari Allah Swt. Semoga kalian akan terus bersama hingga akhir hayat. Semoga kalian akan bersama pula di dalam surga-Nya kelak. Aamiin
												</p>
											</blockquote>
										</div>
									</div>
									<div class="item">
										<div class="testimony-slide text-center">
											<figure>
												<img src="images/friend_wish/fawas.png" alt="user">
											</figure>
											<span>Hilmy Fawas, via <a class="twitter">Whats App</a></span>
											<blockquote>
												<p>
													Alhamdulillah mabroo.. mudahan dilancarkan sampai acara selesai. Jadi keluarga Sakinah Mawaddah Warrahmah. Cepet punya Ganug Junior. Jadi anak yg sholeh sholehah. Kalau udah gede bisa main game ga ngecheat kaya bapanya
												</p>
											</blockquote>
										</div>
									</div>
									<div class="item">
										<div class="testimony-slide text-center">
											<figure>
												<img src="images/friend_wish/fatimah.png" alt="user">
											</figure>
											<span>Fatima Handicraft, via <a href="https://shopee.co.id.fatimahandicraft" target="blank" class="twitter">Whats App, Shoope</a></span>
											<blockquote>
												<p>
													"Semoga SAMAWA kelak dikaruniai keturunan yg Sholeh-sholikhah Aamiin Aamiin Aamiin Ya Rabb 🤲."
												</p>
											</blockquote>
										</div>
									</div>
									<div class="item">
										<div class="testimony-slide text-center">
											<figure>
												<img src="images/friend_wish/anang.png" alt="user">
											</figure>
											<span>Nang Ning, via <a href="#" class="twitter">Twitter</a></span>
											<blockquote>
												<p>"Selamat menikah My Brother a.k.a Dian Nugroho a.k.a Sobatku a.k.a Danug
													Jadilah laki - laki yang bertanggung jawab dan siap dengan segala sesuatu yang sudah ditakdirkan"</p>
											</blockquote>
										</div>
									</div>
									<div class="item">
										<div class="testimony-slide text-center">
											<figure>
												<img src="images/friend_wish/yuda.png" alt="user">
											</figure>
											<span>Yuda Fahrudin, via <a href="#" class="twitter">Whats App</a></span>
											<blockquote>
												<p>"Semoga Allah memberikan keberkahan untukmu dan atasmu, serta semoga
													Dia mengumpulkan di antara kalian berdua dalam kebaikan."</p>
											</blockquote>
										</div>
									</div>
									<div class="item">
										<div class="testimony-slide text-center">
											<figure>
												<img src="images/friend_wish/asmo.png" alt="user">
											</figure>
											<span>Asmo Setiawan, via <a href="#" class="twitter">Valen</a></span>
											<blockquote>
												<p>
													"“Dan diantara tanda-tanda kekuasaanNya ialah Dia menciptakan
													untukmu isteri-isteri dari jenismu sendiri, supaya kamu cenderung
													dan merasa tenteram kepadanya, dan dijadikanNya diantaramu rasa
													kasih dan sayang. Sesungguhnya pada yang demikian itu benar-benar
													terdapat tanda-tanda bagi kaum yang berpikir.” [QS. Ar. Ruum
													(30):21]."
												</p>
											</blockquote>
										</div>
									</div>
								</div>
							</div>
						</div>
						<a href="https://wa.me/+6289667668888?text=wedding.nugroho.xyz \nHello I Wish you,">
							<div class="col-md-8 col-md-offset-2 text-center">
								<!-- <div class="col-md-6 col-sm-6 text-center"> -->
								<button type="submit" class="btn btn-default btn-block">Give Me
									Wishes</button>
							</div>
						</a>
					</div>
				</div>
			</div>
		</div>

		<!-- VIDEO -->
		<div id="fh5co-services" class="fh5co-section-gray">
			<div class="container">
				<div class="row animate-box">
					<div class="col-md-8 col-md-offset-2 text-center fh5co-heading">
						<h2>Our Video Story</h2>
						<p>our short video story made with love at hutan pinus mojokerto.</p>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="feature-left animate-box" data-animate-effect="fadeInLeft">
							<span class="icon">
								<i class="icon-calendar"></i>
							</span>
							<div class="feature-copy">
								<h3>Organized Events</h3>
								<p>Hutan Pinus Pacet Mojokerto.</p>
							</div>
						</div>

						<div class="feature-left animate-box" data-animate-effect="fadeInLeft">
							<span class="icon">
								<i class="icon-image"></i>
							</span>
							<div class="feature-copy">
								<h3>Photoshoot</h3>
								<p>
									Erfan Muji Burahman. <br>
									Indra Lukita.
								</p>
							</div>
						</div>

						<div class="feature-left animate-box" data-animate-effect="fadeInLeft">
							<span class="icon">
								<i class="icon-video"></i>
							</span>
							<div class="feature-copy">
								<h3>Video Editing</h3>
								<p>Erfan Muji Burahman.</p>
							</div>
						</div>

					</div>

					<div class="col-md-6 animate-box">
						<div class="fh5co-video fh5co-bg" style="background-image: url(videos/image_background01.png); ">
							<a href="videos/preweed01.mp4" class="popup-vimeo"><i class="icon-video2"></i></a>
							<div class="overlay"></div>
						</div>
					</div>
				</div>
			</div>
		</div>


		<div id="fh5co-started" class="fh5co-bg" style="background-image:url(images/img_bg_4.jpg);">
			<!-- <div id="fh5co-testimonial"> -->

			<div class="overlay"></div>
			<div class="container">
				<div class="row animate-box">
					<div class="col-md-8 col-md-offset-2 text-center fh5co-heading">
						<h2>Are You Attending?</h2>
						<p>Please Fill-up the form to notify you that you're attending. Thanks.</p>
					</div>
				</div>
				<div class="row animate-box">
					<div class="col-md-10 col-md-offset-1">
						<!-- <form class="form-inline" action="https://wa.me/+6289667668888?text=Hello, \nI will attend. \nDon't forget to invite me">
							<div class="col-md-4 col-sm-4">
								<div class="form-group">
									<label for="name" class="sr-only">Name</label>
									<input type="name" class="form-control" id="name" placeholder="Name">
								</div>
							</div>
							<div class="col-md-4 col-sm-4">
								<div class="form-group">
									<label for="message" class="sr-only">Message</label>
									<input type="message" class="form-control" id="message" placeholder="Message">
								</div>
							</div>
							<div class="col-md-4 col-sm-4">
								<button type="submit" class="btn btn-default btn-block">I am Attending</button>
							</div> 
						</form> -->
						<a href="https://wa.me/+6289667668888?text=Hello, %0aI will attend. %0aDon't forget to invite me" target="blank">
							<div class=" col-md-offset-4 col-md-4 col-sm-4">
								<button type="submit" class="btn btn-default btn-block">I am Attending</button>
							</div>
						</a>
					</div>
				</div>
			</div>
		</div>


		<!-- ======= Contact Section ======= -->
		<div id="fh5co-testimonial">
			<div class="container">
				<div class="row">
					<div class="row animate-box">
						<div class="col-md-8 col-md-offset-2 text-center fh5co-heading">
							<span>Location</span>
							<h2>Weeding Address</h2>
						</div>
					</div>

					<div class="row text-center">
						<!-- <div>
							<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3956.02780599379!2d112.45846005114083!3d-7.462175494590444!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e780df96ae511ef%3A0x1be5fb5aefecea88!2spondok%20programmer%20mojokerto!5e0!3m2!1sid!2sid!4v1599494480134!5m2!1sid!2sid" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
						</div> -->
						<!-- ======= Contact Section ======= -->
						<!-- <section id="contact" class="contact col-md-8 col-md-offset-2 text-center"> -->
						<!-- <div class="overlay"></div>
						<div class="container">
							<div>
								<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3956.02780599379!2d112.45846005114083!3d-7.462175494590444!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e780df96ae511ef%3A0x1be5fb5aefecea88!2spondok%20programmer%20mojokerto!5e0!3m2!1sid!2sid!4v1599494480134!5m2!1sid!2sid" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
							</div>
							<div class="row mt-12">
								<div class="col-lg-12">
									<div class="row">
										<div class="col-md-6">
											<div class="info-box">
												<i class="bx bx-map"></i>
												<h3>Our Address</h3>
												<p>Jl. Sekar Putih No.591, Mergelo, Rt3 Rw1, Kec. Magersari, Kota Mojokerto, Jawa Timur 61316</p>
											</div>
										</div>
										<div class="col-md-3">
											<div class="info-box mt-3">
												<i class="bx bx-envelope"></i>
												<h3>Email Us</h3>
												<p>dian@nugroho.xyz<br>dian@arduinug.com</p>
											</div>
										</div>
										<div class="col-md-3">
											<div class="info-box mt-3">
												<i class="bx bx-phone-call"></i>
												<h3>Call Us</h3>
												<p>
													<a href="https://wa.me/+6289667668888">
														+62 8966 766 8888 ( GaNuG )
													</a>
													<br>
													<a href="https://wa.me/+628988888320">
														+62 898 8888 320 ( Dian )
													</a>
													<br>
												</p>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div> -->
						<div class="overlay"></div>
						<div class="container">
							<div>
								<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d989.0721450385445!2d112.45930618813871!3d-7.433284899663176!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e780f34ed3afdc5%3A0x433b0041a870d13d!2sNudhy%20Collections!5e0!3m2!1sid!2sid!4v1602210542544!5m2!1sid!2sid" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
							</div>
							<div class="row mt-12">
								<div class="col-lg-12">
									<div class="row">
										<div class="col-md-6">
											<div class="info-box">
												<i class="bx bx-map"></i>
												<h3>Our Address</h3>
												<p>Dusun Sonosari RT. 01 RW. 01, Desa. Canggu, Kec. Jetis, Kabupaten Mojokerto</p>
											</div>
										</div>
										<div class="col-md-3">
											<div class="info-box mt-3">
												<i class="bx bx-envelope"></i>
												<h3>Email Us</h3>
												<p>dian@idsuite.my.id<br>dini@nugroho.xyz</p>
											</div>
										</div>
										<div class="col-md-3">
											<div class="info-box mt-3">
												<i class="bx bx-phone-call"></i>
												<h3>Call Us</h3>
												<p>
													<a href="https://wa.me/+6289667668888">
														+62 89 88888 320 ( Dian )
													</a>
													<br>
													<a href="https://wa.me/+628988888320">
														+62 89 88888 640 ( Dini )
													</a>
													<br>
												</p>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- </section> -->
			<!-- End Contact Section -->

			<footer id="fh5co-footer" role="contentinfo">
				<div class="container">

					<div class="row copyright">
						<div class="col-md-12 text-center">
							<!-- <p>
							<small class="block">&copy; 2016 Free HTML5. All Rights Reserved.</small>
							<small class="block">Designed by <a href="http://freehtml5.co/"
									target="_blank">FREEHTML5.co</a> Demo Images: <a href="http://unsplash.co/"
									target="_blank">Unsplash</a></small>
						</p> -->
							<p>
							<ul class="fh5co-social-icons">
								<li><a><i class="icon-twitter"></i></a></li>
								<li><a><i class="icon-facebook"></i></a></li>
								<li><a><i class="icon-linkedin"></i></a></li>
								<li><a><i class="icon-dribbble"></i></a></li>
							</ul>
							</p>
						</div>
					</div>

				</div>
			</footer>
		</div>

		<div class="gototop js-top">
			<a href="#" class="js-gotop"><i class="icon-arrow-up"></i></a>
		</div>

		<!-- jQuery -->
		<script src="js/jquery.min.js"></script>
		<!-- jQuery Easing -->
		<script src="js/jquery.easing.1.3.js"></script>
		<!-- Bootstrap -->
		<script src="js/bootstrap.min.js"></script>
		<!-- Waypoints -->
		<script src="js/jquery.waypoints.min.js"></script>
		<!-- Carousel -->
		<script src="js/owl.carousel.min.js"></script>
		<!-- countTo -->
		<script src="js/jquery.countTo.js"></script>

		<!-- Stellar -->
		<script src="js/jquery.stellar.min.js"></script>
		<!-- Magnific Popup -->
		<script src="js/jquery.magnific-popup.min.js"></script>
		<script src="js/magnific-popup-options.js"></script>

		<!-- // <script src="https://cdnjs.cloudflare.com/ajax/libs/prism/0.0.1/prism.min.js"></script> -->
		<script src="js/simplyCountdown.js"></script>
		<!-- Main -->
		<script src="js/main.js"></script>

		<script>
			// var d = new Date(new Date().getTime() + 200 * 120 * 120 * 2000);
			var d = new Date("November 23, 2020 14:00:00");
			console.log(d);


			// default example
			simplyCountdown('.simply-countdown-one', {
				year: d.getFullYear(),
				month: d.getMonth() + 1,
				day: d.getDate()
			});

			//jQuery example
			$('#simply-countdown-losange').simplyCountdown({
				year: d.getFullYear(),
				month: d.getMonth() + 1,
				day: d.getDate(),
				enableUtc: false
			});
		</script>
</body>

</html>